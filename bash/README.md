# Bash & curl

## Références

- [The AT Protocol](https://atproto.com/docs)
- [Bluesky/AT Protocol: cURL API Interaction Cheatsheet](https://gist.github.com/pojntfx/72403066a96593c1ba8fd5df2b531f2d)

## pré-requis

- binaire [curl](https://curl.se/) - pour faire les requêtes
- binaire [jq](https://jqlang.github.io/jq/) - pour parser json
- un compte Bluesky - bah ouais
- un [mot de passe d'application](https://bsky.app/settings/app-passwords) bluesky

> 📍 Le mot de passe d'application est un autre mot de passe associé à votre compte principal, permet entre autre de ne pas utiliser votre mot de passe principal pour des applications tierces, et de créer un mot de passe par appli (si une appli se fait trouer, vos autres applis et votre compte restent safe 👍)  

## Récupérer son DID à partir de son Handle

La première opération consiste à récupérer un identifiant (DID), qui va être différent de votre nom d'utilisateur/trice (Handle)  
Votre **domain name handle** est de type *pseudo.bsky.social*, alors que votre **DID** ressemble plutôt à *did:plc:bv6ggxxxxxxxxxxxxhnal*  
> 📍 Références : [The AT Protocol - Identity](https://atproto.com/guides/identity)   

L'API s'attendant à recevoir un couple DID / mot de passe, il faut donc faire une première requête pour votre DID

On commence par positionner quelques variables pour plus de lisibilité :  

```bash
export BSKY_H="votre pseudo" # votre handle complet, exemple : raclepoulpe.bsky.social
export DID_URL="https://bsky.social/xrpc/com.atproto.identity.resolveHandle" # Le endpoint d'API pour récupérer le DID en focntion de votre handle
```

Une requête de base avec curl se fait tel que :  

```bash
curl -G --data-urlencode "handle=$BSKY_H" "$DID_URL"
```

et vous obtenez comme résultat un truc du genre :  

```json
{"did":"did:plc:hdfpsl4hkbggutlw4adea5xp"}
```

Pour extraire la partie qui nous intéresse, on utilise le (génial) outils jq et on met le résultat dans une variable :  

```bash
export DID=$(curl -G --data-urlencode "handle=$BSKY_H" "$DID_URL" | jq -r .did)
```

On peut vérifier que la variable contienne le bon résultat : 
```bash
echo $DID
did:plc:hdfpsl4hkbggutlw4adea5xp
```

## Récupérer un token d'identification

Pour requêter une API, on ne renvoie pas à chaque fois son user / password (désolé pour les anglissismes), qui ne seront utilisés qu'à l'ouverture d'une session, afin d'obtenir un jeton d'authentification (token) qui nous autorisera à faire nos requêtes.  
Ce token ici est un token [JWT](https://jwt.io/), représenté par une longue chaîne de caractères.  
> 📍 On le retrouve souvent dans les requêtes curl comme valeur du champ **Bearer** du paramètre **Authentication**, exemple : "Authorization: Bearer {token}"  

Mais ici c'est différent, le couple d'authentification user /password est une composante **data** d'une requête de type **POST** sur le endpoint **com.atproto.server.createSession** de cette forme :  

```json
{ "identifier" : "DID" , "password": "application password" }
```

On va repositionner quelques variables, toujours pour plus de visibilité : 

```bash
export BSKY_P="application password" # celui généré via vos paramètres
export API_KEY_URL='https://bsky.social/xrpc/com.atproto.server.createSession' # Le endpoint de création de session de l'API
export POST_DATA="{ \"identifier\" : \"${DID}\" , \"password\": \"${BSKY_P}\" }" # Le bloc data de la requête curl
```

On peut tester la requête (avec jq pour la mise en forme uniquement) : 

```bash
curl -X POST -H 'Content-Type: application/json' -d "$POST_DATA" "$API_KEY_URL" | jq
```

et obtenir le résultat :

```json
{
  "did": "did:plc:hdfpsl4hkbggutlw4adea5xp",
  "handle": "raclepoulpe.bsky.social",
  "email": "raclepoulpe@somemail.com",
  "accessJwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6ImNvbS5hdHByb3RvLmFwcFBhcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxV0bHc0YWRlYTV4cCIsImlhdCI6MTY5MjEwNzA1OCwiZXhwIjoxNjkyMTE0MjU4fQ.PlVvo0rnALLXanCDGKascrvBnGdEObgf2YE7GwtUx0k",
  "refreshJwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6ImNvbS5hdHByb3RvLnJlZnJxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxRoa2JnZ3V0bHc0YWRlYTV4cCIsImp0aSI6ImpmYnJXMFQ0cjVha0piVkgyT29VeXM5WDZYMDFja1REZ0pDWGRURDBsSlkiLCJpYXQiOjE2OTIxMDcwNTgsImV4cCI6MTY5OTg4MzA1OH0.2TszoBIZeVIFcn3GFZoxaH10fRgFW0Vz-V-OdLkTwi4"
}
```

On peut maintenant extraire le token jwt et l'intégrer dans une variable : 

```bash
export API_KEY=$(curl -X POST -H 'Content-Type: application/json' -d "$POST_DATA" "$API_KEY_URL" | jq -r .accessJwt)
```

Votre token est dispo via API_KEY :  
```bash
echo $API_KEY
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6ImNvbS5hdHByb3RvLmFwcFBhcxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxV0bHc0YWRlYTV4cCIsImlhdCI6MTY5MjEwNzA1OCwiZXhwIjoxNjkyMTE0MjU4fQ.PlVvo0rnALLXanCDGKascrvBnGdEObgf2YE7GwtUx0k
```

## Requête de lecture

Pour les différents exemples, je me base sur la [doc officielle](https://atproto.com/).  
Elle est organisée en 3 parties : Guides, specs et lexicon.  
Celle qui nous intéresse est [lexicon](https://atproto.com/specs/lexicon), la définition des HTTP endpoints (XRPC).  

### Exemple n°1 : récupérer le contenu de sa timeline

Endpoint : [app.bsky.feed.getTimeline](https://atproto.com/lexicons/app-bsky-feed#appbskyfeedgettimeline)   

On va récupérer avec une requête le contenu de sa timeline.  

Tout d'abord, on créé une variable pour le endpoint : 

```bash
export TIMELINE_URL="https://bsky.social/xrpc/app.bsky.feed.getTimeline"
```

Ensuite, on regarde dans la structure du endpoint quels sont les paramètres à passer à notre requête (en plus du token d'authentification récupéré + haut)  

```json
...
      "parameters": {
        "type": "params",
        "properties": {
          "algorithm": {
            "type": "string"
          },
          "limit": {
            "type": "integer",
            "minimum": 1,
            "maximum": 100,
            "default": 50
          },
          "cursor": {
            "type": "string"
          }
        }
      }
...      
```

Ici, aucun paramètre obligatoire (sinon il y'aurait un "required": [ xxx ]), et on peut agir sur **algorithm**, **limit** et **cursor**.  
> 📍 A l'heure où j'écris ce tuto, je n'ai pas encore assez exploré les notions d'algorythmes et de cursors pour en parler ici, je passerai donc sur ces deux options.

On peut faire la requête tel que : 

```bash
curl -G -H "Authorization: Bearer ${API_KEY}" "$TIMELINE_URL" | jq
```

et vous obtenez le contenu de votre timeline au format json.  

Pour limiter le nombre d'éléments en sortie, deux possibilités :  
- utiliser le cursor (à voir + tard)
- limiter le nombre de réponses  

Pour limiter le nombre de réponse, on positionne une valeur de **3** par exemple au pramètre **limit** :  

```bash
curl -G -H "Authorization: Bearer ${API_KEY}" --data-urlencode "limit=3" "$TIMELINE_URL" | jq
```

> 💡 Globalement, pour ajouter un paramètre à une requête, ajoutez à votre commande la chaîne de caractère suivante :  
> ```bash
> --data-urlencode "(parametre)=(valeur)"
> ```

### Exemple n°2 : récupérer la liste de ses followers

On va reprendre un peu les mêmes principes que l'exemple n°1, on va juste changer l'adresse du [endpoint](https://atproto.com/lexicons/app-bsky-graph#appbskygraphgetfollowers) et modifier les paramètres d'entrée.  
On intègre aussi une nouvelle variable **ACTOR**, qui est en gros un user Bluesky, au format handle.  
Pour plus de lisibilité, on va limiter le nombre de réponse à 5 via le paramètre **limit**

```bash
export FOLLOWERS_URL="https://bsky.social/xrpc/app.bsky.graph.getFollowers"
export ACTOR="raclepoulpe.bsky.social"
```

Et on exécute la réquête, en filtrant un minimum, toujours avec jq : 

```bash
curl -G -H "Authorization: Bearer ${API_KEY}" --data-urlencode "actor=$ACTOR" --data-urlencode "limit=5" "$FOLLOWERS_URL" | jq .followers[]
```
Exemple de réponse, un bloc complet : 

```json
{
  "did": "did:plc:if5s47e4ggz63mzvkxfcs6uw",
  "handle": "mwyann.fr",
  "displayName": "Yann",
  "description": "Développement informatique, jeux vidéo, geekeries, bricolage et DIY. #bzh 🇫🇷\n@Mwyann@twitter.com et @Mwyann@mastodon.social",
  "avatar": "https://cdn.bsky.social/imgproxy/kpw5MqAYT-NM-33vIW617Zkw2BS0zwy4mo_WV3pLEzA/rs:fill:1000:1000:1:0/plain/bafkreia2spi5mbng3jnnjcfemyj4iyilzfdakltsxoubhezxvwtepdilgq@jpeg",
  "indexedAt": "2023-07-17T20:59:21.861Z",
  "viewer": {
    "muted": false,
    "blockedBy": false,
    "following": "at://did:plc:hdfpsl4hkbggutlw4adea5xp/app.bsky.graph.follow/3k4yyxvlkgp2g",
    "followedBy": "at://did:plc:if5s47e4ggz63mzvkxfcs6uw/app.bsky.graph.follow/3k4ywkc7nsd2d"
  }
```

et pour aller un peu plus loin avec jq, on peut filtrer sur les **did**, **handle**, **displayName** et le bloc viewer que je trouve intéressant :  

```bash
curl -G -H "Authorization: Bearer ${API_KEY}" --data-urlencode "actor=$ACTOR" --data-urlencode "limit=5" "$FOLLOWERS_URL" | jq '.followers[] | {did, handle , displayName, viewer}'
```

Le résultat : 

```json
{
  "did": "did:plc:if5s47e4ggz63mzvkxfcs6uw",
  "handle": "mwyann.fr",
  "displayName": "Yann",
  "viewer": {
    "muted": false,
    "blockedBy": false,
    "following": "at://did:plc:hdfpsl4hkbggutlw4adea5xp/app.bsky.graph.follow/3k4yyxvlkgp2g",
    "followedBy": "at://did:plc:if5s47e4ggz63mzvkxfcs6uw/app.bsky.graph.follow/3k4ywkc7nsd2d"
  }
}
{
  "did": "did:plc:vq6qocflnorbiqgft5n5cidz",
  "handle": "d4r.pw",
  "displayName": "io•d4r report",
  "viewer": {
    "muted": false,
    "blockedBy": false,
    "followedBy": "at://did:plc:vq6qocflnorbiqgft5n5cidz/app.bsky.graph.follow/3k4ynbp7obe2u"
  }
}
{
  "did": "did:plc:u33wacfom75lvpov5cajs7zv",
  "handle": "tixlegeek.bsky.social",
  "displayName": "☠ 𝗧𝗶𝘅𝗹𝗲𝗴𝗲𝗲𝗸 ⚒",
  "viewer": {
    "muted": false,
    "blockedBy": false,
    "following": "at://did:plc:hdfpsl4hkbggutlw4adea5xp/app.bsky.graph.follow/3k2sw7x7t462o",
    "followedBy": "at://did:plc:u33wacfom75lvpov5cajs7zv/app.bsky.graph.follow/3k4x57uqwwr2j"
  }
}
{
  "did": "did:plc:tgbnsncxdv6vysfstgbscesc",
  "handle": "cedricgatay.bsky.social",
  "displayName": null,
  "viewer": {
    "muted": false,
    "blockedBy": false,
    "followedBy": "at://did:plc:tgbnsncxdv6vysfstgbscesc/app.bsky.graph.follow/3k4wslbuyfp2c"
  }
}
{
  "did": "did:plc:lakuxsrt34wcxbiz6b3rknme",
  "handle": "sudarshan-anand.bsky.social",
  "displayName": "Sudarshan Anand",
  "viewer": {
    "muted": false,
    "blockedBy": false,
    "followedBy": "at://did:plc:lakuxsrt34wcxbiz6b3rknme/app.bsky.graph.follow/3k4wrr4zgon2j"
  }
}
```

On commence à voir des éléments de réponses intéressants, comme **following** et **followedBy**  
Je suppose que ce sont des adresses qui pointent vers des listes, les followers et les followed, ce qui serait parfait pour élaborer des graphs de relations 🤩 mais je n'ai pas encore trouvé comment les exploiter (stay tuned!)

### Exemple n°3 : Récupérer le nombre de notifications

Bon, vous connaissez le principe maintenant, je ne mets que les éléments qui changent.  

```bash
export UNREAD_URL="https://bsky.social/xrpc/app.bsky.notification.getUnreadCount"
curl -s -G -H "Authorization: Bearer ${API_KEY}" "$UNREAD_URL" | jq
```

résultat : 

```json
{
  "count": 1
}
```



## Requête d'écriture

*TODO*
