# Bluesky - bidouilles & co

Ici quelques essais / trucs / bricolages autours de l'API [Bluesky](https://bsky.app/), et plus particuliérement autour de [AT Protocol](https://atproto.com/)  
Note : c'est de la bidouille, et créé sans grandes prétentions, juste pour partager, les remarques constructives sont les bienvenues, les PR encore plus 😉

## Bash / curl

C'est toujours un de mes points de départ pour tester, avant de passer sur un langage + structuré  

- 🖥 [Rubrique BASH & Curl](./bash/README.md) 

